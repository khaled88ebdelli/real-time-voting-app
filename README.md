# Real time voting app

A full stack application to vote in real time

### ToDo

- :heavy_check_mark: Setup Server
  - :heavy_minus_sign: Install Dependencies
  - :heavy_minus_sign: Install / Setup Linter
  - :heavy_minus_sign: Setup Express App
  - :heavy_minus_sign: Setup Not Found and Error Middlewares